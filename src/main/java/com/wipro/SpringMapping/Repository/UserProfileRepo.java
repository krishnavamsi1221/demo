package com.wipro.SpringMapping.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wipro.SpringMapping.Entity.UserProfile;

public interface UserProfileRepo extends JpaRepository<UserProfile, Long> {

}
