package com.wipro.SpringMapping.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wipro.SpringMapping.Entity.User;
import com.wipro.SpringMapping.Repository.UserRepo;

@Service
public class UserService {

	@Autowired
	private UserRepo repo;

	public User addUser(User user) {
		return repo.save(user);
	}

	public List<User> getUsers() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

}
